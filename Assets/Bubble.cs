﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite[] bubbles;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Sprite hunger()
    {
        return bubbles[0];
    }
    public Sprite party()
    {
        return bubbles[1];
    }

    public Sprite paper()
    {
        return bubbles[2];
    }

}
