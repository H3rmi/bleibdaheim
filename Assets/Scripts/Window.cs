﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 location;
    public bool actuallyUsed;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 GetLocation()
    {

        return gameObject.transform.position + location;
    }

    public void Open()
    {
        actuallyUsed = true;
        GetComponent<SpriteRenderer>().enabled = false;
    }

    public void Close()
    {
        actuallyUsed = false;
        GetComponent<SpriteRenderer>().enabled = true;
    }
}
