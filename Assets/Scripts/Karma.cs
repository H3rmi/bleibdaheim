﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Karma : MonoBehaviour
{
    
    private int karmaValue { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        karmaValue = 100;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeKarma(int karma)
    {
        karmaValue += karma;
    }
}
