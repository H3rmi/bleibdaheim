﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    public bool canGetDragged = false;
    public bool mouseDown { private set; get; }
    private bool mouseOver = false;

    public bool isOverHome = false;
    private Interactable _interactable;
    private Interaction interactionScript;

    private Person personScript;

    // Start is called before the first frame update
    void Start()
    {
        personScript = gameObject.GetComponent<Person>();
        _interactable = gameObject.GetComponent<Interactable>();
        interactionScript = FindObjectOfType<Interaction>();
        mouseDown = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (mouseDown && canGetDragged && interactionScript.isDragging)
        {
            var newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newPos.z = transform.position.z;
            gameObject.transform.position = newPos;
            if (personScript.home)
            {
                personScript.home._interactable.SoftHighlight(Color.magenta);
            }
        }
        else if (mouseOver && !interactionScript.isDragging)
        {
           _interactable.Highlight(Color.cyan);
        }
        else
        {
            _interactable.Highlight();
        }

        if (!mouseDown && Input.GetMouseButtonUp(1) && interactionScript.actualTarget == gameObject)
        {
            interactionScript.StopInteract();
        }
    }

    public void SetHome(CharacterHome home)
    {
        if (!personScript)
        {
            personScript = gameObject.GetComponent<Person>();
        }
        personScript.home = home;
    }

    void OnMouseDown()
    {
        mouseDown = true;
        interactionScript.StartInteract(gameObject);
    }

    void OnMouseUp()
    {
        mouseDown = false;
        interactionScript.StopInteract();
    }

    void OnMouseEnter()
    {
        Debug.Log("Enter");
        mouseOver = true;
        if (!interactionScript.isDragging || mouseDown)
        {
            _interactable.Highlight(Color.cyan);
        }
    }

    void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(1) && !interactionScript.isDragging){
            interactionScript.StartInteract(gameObject, true);
        }

        if (Input.GetMouseButtonUp(1) && !mouseDown)
        {
            interactionScript.StopInteract();
        }
    }

    void OnMouseExit()
    {
        mouseOver = false;
        if (!mouseDown)
        {
            _interactable.Highlight();
        }
    }
}
