﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundLoop : MonoBehaviour
{

    public GameObject background;
    public float cameraSpeed = 1;
    private Camera mainCamera;

    public delegate void Teleport(Vector3 CameraPos);

    public Teleport TeleportDelegate;
    private Vector2 screenBounds;

    private float Space;

    public float backgroundWidth { get; private set; }

    private Transform[] bgChildren;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = gameObject.GetComponent<Camera>();
        screenBounds =
            mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, mainCamera.transform.position.z));
       // foreach (var obj in levelsObject)
        {
            loadChildObjects(background);
        }
        Space = Screen.width / 5;

    }

    void loadChildObjects(GameObject obj)
    {
        backgroundWidth = obj.GetComponent<SpriteRenderer>().bounds.size.x;
        int childsNeeded = (int)Mathf.Ceil(screenBounds.x * 2 / backgroundWidth);
        GameObject clone = Instantiate(obj);
        for (int i = 0; i <= childsNeeded; i++)
        {
            GameObject c = Instantiate(clone);
            c.transform.SetParent(obj.transform);
            c.transform.position = new Vector3(backgroundWidth * i,obj.transform.position.y, obj.transform.position.z);
            c.name = obj.name + i;
        }
        Destroy(clone);
        Destroy(obj.GetComponent<SpriteRenderer>());
        bgChildren = obj.GetComponentsInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        var mousePos = Input.mousePosition;
        Vector3 campos = mainCamera.transform.position;

        if (Input.GetKey("d") || mousePos.x >= Screen.width - Space)
        {
            campos.x += cameraSpeed * Time.deltaTime;
        }

        if (Input.GetKey("a") || mousePos.x <= Space)
        {
            campos.x -= cameraSpeed * Time.deltaTime;
        }

        mainCamera.transform.position = campos;
    }

    void LateUpdate()
    {

        if (transform.hasChanged)
        {
            repositionCamera(background);
        }
    }


    private void repositionCamera(GameObject o)
    {
        //Transform[] children = o.GetComponentsInChildren<Transform>();
        if (bgChildren.Length > 1)
        {
            Vector3 camPos = mainCamera.transform.position;
            GameObject first = bgChildren[1].gameObject;
            GameObject last = bgChildren[bgChildren.Length -1].gameObject;
            float halfBackWidth = backgroundWidth * 0.5f;
            if (transform.position.x + screenBounds.x > last.transform.position.x + halfBackWidth)
            {
                camPos.x -= backgroundWidth;
                mainCamera.transform.position = camPos;
                TeleportDelegate(camPos);

            }
            else if (transform.position.x - screenBounds.x < first.transform.position.x - halfBackWidth)
            {
                camPos.x += backgroundWidth;
                mainCamera.transform.position = camPos;
                TeleportDelegate(camPos);
            }
        }
    }
}
