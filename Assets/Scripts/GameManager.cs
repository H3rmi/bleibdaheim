﻿using System.Collections;
using System.Collections.Generic;
using Assets.ExampleAssets.BleibDaheim;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    private GameTimer timer;

    public static GameManager instance { get; private set; }
    private bool gameHasStarted = false;

    void Start()
    {
        timer = new GameTimer();
        instance = this;
        GameStart();
    }

    void GameStart()
    {
        gameHasStarted = true;
    }

    public GameTimer getTimer()
    {
        return timer;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameHasStarted)
        {
            timer.Decrease(Time.deltaTime);
        }
    }

    public void IncreaseTime(float time)
    {
        timer.Decrease(-time);
    }

    public void DecreaseTime(float time)
    {
        timer.Decrease(time);
    }
}