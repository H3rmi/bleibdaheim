﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.ExampleAssets.BleibDaheim
{
    public class GameTimer
    {
        private static GameTimer instance;
        private float Timer;

        public GameTimer()
        {
            if (instance != null)
            {
                return;
            }

            Timer = 180.0f;
            instance = this;
        }
        public void Decrease(float time)
        {
            Timer -= time;
            if (Timer <= 0.0f)
            {
                Debug.Log("You loose!");
            }
        }
    }
}
